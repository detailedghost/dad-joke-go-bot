package main

import (
	"errors"
	"flag"
	"math/rand"
	"os"
	"regexp"
	"strings"
	"time"

	"github.com/kevinburke/twilio-go"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"golang.org/x/net/context"
	"thundermane.com/x/daily-dad-joke/internal/env"
	"thundermane.com/x/daily-dad-joke/pkg/clients"
	"thundermane.com/x/daily-dad-joke/pkg/services"
)


var (
	twilioService *services.TwilioClient
	apiClientConfig = clients.ApiInfoClientConfig{
		UserAgent: "Dad-Joke-Bot",
	}
)

var apiClients = map[string]clients.ApiInfoClient{
		"dad-joke": clients.NewDadJokeClient(&apiClientConfig),
		"cat-fact": clients.NewCatFactClient(&apiClientConfig),
		"dog-fact": clients.NewDogFactClient(&apiClientConfig),
	};

type cliCommandsPayload struct{ 
	twilioSid *string
	twilioAuthToken *string
	twilioPhone *string
	phoneNumbers *string
	clientType *string
	useEnv *bool
	showHelpMenu *bool
}

func defineFlags() cliCommandsPayload {
	cliContext := cliCommandsPayload{}
	cliContext.useEnv = flag.Bool("use-env-file", false, "Use a environment file based on environment")
	cliContext.twilioSid = flag.String("twilio-sid", "", "Twilio SID")
	cliContext.twilioAuthToken = flag.String("twilio-token", "", "Twilio Token")
	cliContext.twilioPhone = flag.String("twilio-phone", "", "Phone Twilio is from")
	cliContext.clientType = flag.String("client", "dad-joke", "Select a client type ('dad-joke', 'cat-fact', 'random')")
	cliContext.showHelpMenu = flag.Bool("help", false, "Show Help Menu")
	flag.Parse()

	if *cliContext.showHelpMenu {
		flag.PrintDefaults()
		os.Exit(1)
	}

	phoneNumbers := flag.Arg(0)
	if phoneNumbers == "" { 
		panic("Phone numbers needed!")
	}

	foundValid := regexp.MustCompile("^(\\+?[0-9]{10,},?)*$").MatchString(phoneNumbers)
	if !foundValid { 
		panic("Need valid numbers")
	}

	cliContext.phoneNumbers = &phoneNumbers;
	return cliContext
}

func loadEnv (payload cliCommandsPayload) { 
	env.EnvLoader{
		Logger: &log.Logger,
	}.
		New().
		LoadEnv(os.Getenv("ENVIRONMENT"))

	if *payload.twilioSid == "" { 
		*payload.twilioSid = os.Getenv("TWILIO_ACCOUNT_SID")
	}

	if *payload.twilioAuthToken == "" {
		*payload.twilioAuthToken = os.Getenv("TWILIO_AUTH_TOKEN")
	}

	if *payload.twilioPhone == "" {
		*payload.twilioPhone = os.Getenv("TWILIO_PHONE")
	}
}

func parseCommands() cliCommandsPayload {
	payload := defineFlags()
	if *payload.useEnv { 
		loadEnv(payload)
	}
	return payload;
}

func loadServices(ctx context.Context) error {
	flags, ok := ctx.Value(cliCommandsPayload{}).(cliCommandsPayload);
	if !ok { 
		return errors.New("Invalid Twilio configuration was passed")
	}
	if flags.twilioSid == nil || flags.twilioAuthToken == nil || flags.twilioPhone == nil {
		return errors.New("Invalid Twilio configuration was passed")
	}
	twilioService = services.NewTwilioClient(services.TwilioClientConfig{
		Sid: *flags.twilioSid,
		Token: *flags.twilioAuthToken,
		FromNumber: *flags.twilioPhone,
	});
	return nil
}

func getClientMapKeys() []string {
	opts := make([]string, 0, len(apiClients))
	for k := range apiClients {
		opts = append(opts, k)
	}
	return opts
}

func getClientMessage(ctx context.Context) (clients.ApiInfoClientMessage, error) { 
	payload, pOk := ctx.Value(cliCommandsPayload{}).(cliCommandsPayload)
	if !pOk { 
		return clients.EmptyClientMessage{}, errors.New("Issue finding context payload");
	}

	result, ok := apiClients[*payload.clientType]
	if ok {
		return result.FetchRandom(ctx)
	}

	switch *payload.clientType { 
	case "random":
		rand.Seed(time.Now().UnixNano());
		options := getClientMapKeys()
		selected := options[rand.Intn(len(options))]
		return apiClients[selected].FetchRandom(ctx)
	default: 
		return clients.EmptyClientMessage{}, errors.New("None defined")
	}
}

func sendMessage(ctx context.Context, number string) (*twilio.Message, error) {
	log.Info().Msgf("Sending to %v", number)
	message, err := getClientMessage(ctx)
	if err != nil { 
		log.Err(err).Msg("No message found")
		return nil, err
	}
	return twilioService.SendMessage(number, message.Message())
}

type SendMsgChanPayload struct {
	Msg *twilio.Message
	Err error
}

func sendMessageParallel(ctx context.Context, number string) <-chan SendMsgChanPayload { 
	sender := make(chan SendMsgChanPayload)
	go func() {
		msg, err := sendMessage(ctx, number)
		select { 
		case <-ctx.Done():
			// * Context was canceled, head back
			return;
		case sender<-SendMsgChanPayload{msg, err}:
			// * Successful mission, send back
		}
	}()
	return sender
}

func main() { 
	ctx, cancel := context.WithTimeout(context.Background(), 5 * time.Minute)
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	defer cancel()

	ctx = context.WithValue(ctx, cliCommandsPayload{}, parseCommands())
	commandPayload := ctx.Value(cliCommandsPayload{}).(cliCommandsPayload);

	loadServices(ctx)
	for _, phoneNum := range strings.Split(strings.Trim(*commandPayload.phoneNumbers, " "), ",") { 
		receiver :=<-sendMessageParallel(ctx, phoneNum)
		if receiver.Err == nil && receiver.Msg != nil {
			log.Info().Msgf("Sent message to %v.", phoneNum)
		} else { 
			log.Warn().Msgf("Issue sending to %v - %v", phoneNum, receiver.Err.Error())
		} 
	}
}
