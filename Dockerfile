FROM golang:1.17

LABEL \
  version="1.0" \
  description="A random fact checker that uses various APIs. Contains a fully fledge API and a CLI tool" \
  org.opencontainers.image.authors="dante@thundermane.com"

ARG TWILIO_ACCOUNT_SID
ARG TWILIO_AUTH_TOKEN
ARG TWILIO_PHONE
ARG ENVIRONMENT=production

ENV TWILIO_ACCOUNT_SID=${TWILIO_ACCOUNT_SID}
ENV TWILIO_AUTH_TOKEN=${TWILIO_AUTH_TOKEN}
ENV TWILIO_PHONE=${TWILIO_PHONE}
ENV ENVIRONMENT=${ENVIRONMENT}

WORKDIR /usr/src/dad-joke-bot
COPY . .

RUN go mod download
RUN go mod verify
RUN go build -v -o /usr/local/bin/dad_bot ./cmd/cli.go

CMD dad_bot