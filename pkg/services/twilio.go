package services

import (
	twilio "github.com/kevinburke/twilio-go"
)

type TwilioClientConfig struct {
	Sid, Token, FromNumber string
}

type TwilioClient struct{
	Config TwilioClientConfig
	tw *twilio.Client
}

func NewTwilioClient(config TwilioClientConfig) *TwilioClient {
	client := TwilioClient{config, nil}
	if config.Sid != "" && config.Token != "" { 
		client.tw = twilio.NewClient(config.Sid, config.Token, nil);
	}
	return &client
}

func (client *TwilioClient) SendMessage(phone string, message string) (*twilio.Message, error) {
	return client.tw.Messages.SendMessage(client.Config.FromNumber, phone, message, nil)
}
