package clients

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
)

type GistClientMessageFile struct{ 
	Filename string `json:"filename"`
	Type string `json:"type"`
	Language string `json:"language"`
	RawUrl string `json:"raw_url"`
	Size uint64 `json:"size"`
	Truncated bool `json:"truncated"`
}

type GistClientMessage struct {
	Owner string `json:"owner.id"`
	Files map[string]GistClientMessageFile
}

func (m GistClientMessage) Message() string {
	return fmt.Sprintf("Test \n> From Github Gist - %v", m.Owner)
}

type GistPayload struct{ 
	Id string
	Filename string
}

type GistClient struct {
	UserAgent string
	Logger *log.Logger
	Gist GistPayload
	baseUrl string
	headers map[string]string
}

func (c GistClient) New() GistClient {
	c.baseUrl = "https://api.github.com/gists/"
	if c.Logger == nil {
		c.Logger = log.Default()
	}
	if c.UserAgent == "" {
		c.UserAgent = "Go Random Gist Fact"
	}
	c.headers["Accept"] = "application/vnd.github.v3+json"
	return c;
}

func addBasicHeaders(headers map[string]string, request *http.Request) {
	for k, v := range headers { 
		request.Header.Add(k,v)
	}
}

func (c GistClient) FetchRandom() (ApiInfoClientMessage, error) { 
	req, _ := http.NewRequest("GET", c.baseUrl + c.Gist.Id, nil)
	addBasicHeaders(c.headers, req)
	res, resErr := http.DefaultClient.Do(req)
	if resErr != nil { 
		return GistClientMessage{}, errors.New("Gist request failed")
	}
	var msg GistClientMessage
	json.NewDecoder(res.Body).Decode(&msg)
	return msg, nil
}
