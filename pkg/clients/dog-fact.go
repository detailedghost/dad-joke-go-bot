package clients

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

type DogFact struct { 
	Fact []string `json:"facts"`
	Success bool `json:"success"`
}

func (f DogFact) String() string {
	return fmt.Sprintf("%v", f.Fact)
}

func (f DogFact) Message() string { 
	return fmt.Sprintf("%v \n> From: dog-api.kinduff.com", f.Fact[0])
}

type DogFactClient struct{ 
	UserAgent string
	Logger *log.Logger
	baseUrl string
}

func NewDogFactClient(config *ApiInfoClientConfig) *DogFactClient {
	client := DogFactClient{config.UserAgent, config.Logger, "https://dog-api.kinduff.com"}

	if client.UserAgent == "" {
		client.UserAgent = "Dad Joke Bot Client"
	}

	if client.Logger == nil { 
		client.Logger = log.Default()
	}

	return &client
}

func (client DogFactClient) FetchRandom(ctx context.Context) (ApiInfoClientMessage, error) { 
	req, err := http.NewRequestWithContext(ctx, "GET", client.baseUrl + "/api/facts", nil)
	if err != nil { 
		return DogFact{}, err
	}

	resp, err := (&http.Client{}).Do(req)
	if err != nil { 
		return DogFact{}, err
	}
	defer resp.Body.Close()


	var fact DogFact
	if err := json.NewDecoder(resp.Body).Decode(&fact); err != nil {
		return DogFact{}, err
	}


	return fact, nil
}
