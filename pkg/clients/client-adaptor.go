package clients

import (
	"context"
	"log"
)


type ApiInfoClientMessage interface {
	Message() string
}

type ApiInfoClient interface {
	FetchRandom(ctx context.Context) (ApiInfoClientMessage, error)
}

type ApiInfoClientConfig struct {
	UserAgent string
	Logger *log.Logger
}

type EmptyClientMessage struct {}
func (msg EmptyClientMessage) Message() string {
	return ""
}

type EmptyClient struct {}
func (ec EmptyClient) FetchRandom() (EmptyClientMessage, error) { 
	return EmptyClientMessage{}, nil
}