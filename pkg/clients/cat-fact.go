package clients

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

type CatFact struct { 
	Fact string `json:"fact"`
	Length uint `json:"length"`
}

func (f CatFact) String() string {
	return fmt.Sprintf("%v | %v", f.Fact, f.Length)
}

func (f CatFact) Message() string { 
	return fmt.Sprintf("%v \n> From: catfact.ninja", f.Fact)
}

type CatFactClient struct{ 
	UserAgent string
	Logger *log.Logger
	baseUrl string
}

func NewCatFactClient(config *ApiInfoClientConfig) *CatFactClient {
	client := CatFactClient{config.UserAgent, config.Logger, "https://catfact.ninja"}

	if client.UserAgent == "" { 
		client.UserAgent = "Dad Joke Bot Client"
	}

	if client.Logger == nil { 
		client.Logger = log.Default()
	}

	return &client
}


func (client CatFactClient) FetchRandom(ctx context.Context) (ApiInfoClientMessage, error) { 
	req, err := http.NewRequestWithContext(ctx, "GET", client.baseUrl + "/fact", nil);
	if err != nil { 
		return CatFact{}, err
	}

	resp, err := (&http.Client{}).Do(req)
	if err != nil { 
		return CatFact{}, err
	}
	defer resp.Body.Close()

	var fact CatFact
	if err := json.NewDecoder(resp.Body).Decode(&fact); err != nil {
		return CatFact{}, err
	}

	return fact, nil
}
