package clients

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"
)

type DadJoke struct{ 
	Id string `json:"id"`
	Joke string `json:"joke"`
	Status int `json:"status"`
}

func (j DadJoke) String() string {
	return fmt.Sprintf("%v - %v | %v", j.Id, j.Status, j.Joke)
}

func (j DadJoke) Message() string { 
	return fmt.Sprintf("%v \n> From: icanhazdadjoke.com", j.Joke)
}


type DadJokeClient struct{
	UserAgent string
	Logger *log.Logger
	baseUrl string
}

func NewDadJokeClient(config *ApiInfoClientConfig) *DadJokeClient {
	client := DadJokeClient{config.UserAgent, config.Logger, "https://icanhazdadjoke.com"}

	if client.UserAgent == "" { 
		client.UserAgent = "Dad Joke Bot Client"
	}

	if client.Logger == nil { 
		client.Logger = log.Default()
	}

	return &client
}

func (client DadJokeClient) FetchRandom(ctx context.Context) (ApiInfoClientMessage, error) {
	httpClient := &http.Client{Timeout: 10 * time.Second}
	req, _ := http.NewRequest("GET", client.baseUrl, nil);
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("UserAgent", client.UserAgent)
	res, resErr := httpClient.Do(req);
	if resErr != nil {
		client.Logger.Println("Failed to grab random joke");
		return DadJoke{}, resErr
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK { 
		client.Logger.Println("A non-ok response was given")
		return DadJoke{}, errors.New("Response was not 200")
	}
	result, err := io.ReadAll(res.Body);
	if err != nil {
		client.Logger.Println("Failed to grab random joke");
		return DadJoke{}, err
	}
	var joke DadJoke
	jokeErr := json.Unmarshal(result, &joke);
	if jokeErr != nil {
		client.Logger.Println("Joke Marshal Method Failed");
		return DadJoke{}, err
	}
	return joke, nil
}