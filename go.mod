module thundermane.com/x/daily-dad-joke

go 1.17

require (
	github.com/joho/godotenv v1.4.0
	github.com/kevinburke/twilio-go v0.0.0-20210327194925-1623146bcf73
	github.com/rs/zerolog v1.26.1
	golang.org/x/net v0.0.0-20210805182204-aaa1db679c0d
)

require (
	github.com/gofrs/uuid v4.2.0+incompatible // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/kevinburke/go-types v0.0.0-20210723172823-2deba1f80ba7 // indirect
	github.com/kevinburke/rest v0.0.0-20210506044642-5611499aa33c // indirect
	github.com/ttacon/builder v0.0.0-20170518171403-c099f663e1c2 // indirect
	github.com/ttacon/libphonenumber v1.2.1 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	google.golang.org/protobuf v1.26.0 // indirect
)
