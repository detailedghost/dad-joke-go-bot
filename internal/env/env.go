package env

import (
	"errors"
	"fmt"
	"path/filepath"
	"runtime"

	"github.com/joho/godotenv"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

type EnvLoader struct { 
	Environment, Directory, Filename string
	FileEnvMap map[string]string
	Logger *zerolog.Logger
}

func (loader EnvLoader) New() *EnvLoader {
	if loader.Logger == nil {
		loader.Logger = zerolog.DefaultContextLogger
	}
	return &loader
}

func (loader *EnvLoader) LoadEnv(envStr string) {
	if envStr != "" {
		loader.Environment = envStr
	}
	envErr := loader.Load()
	if(envErr != nil) { 
		log.Info().Msg("Issue finding environment file")
	}
	if loader.Environment != "" {
		log.Info().Msg(fmt.Sprintf("Running Environment: %v", loader.Environment))
	}
}

func (loader *EnvLoader) Load() error {
	_, filename, _, ok := runtime.Caller(0)
	if !ok {
	    return errors.New("unable to get the current filename (e.g. __filename)")
	}
	cwd := fmt.Sprintf("%v/../..", filepath.Dir(filename))

	path := fmt.Sprintf("%v/configs", cwd);
	loader.Directory = path
	if len(loader.Filename) > 0 {
		path = fmt.Sprintf("%v/%v", loader.Directory, loader.Filename)
	} else if len(loader.Environment) > 0 { 
		path = fmt.Sprintf("%v/.env.%v", loader.Directory, loader.Environment)
	} else {
		path = fmt.Sprintf("%v/.env", loader.Directory)
	}
	loader.Logger.Debug().Msg(path)
	err := godotenv.Load(path);
	if err != nil { 
		loader.Logger.Info().Msg("No environment file found");
		return err;
	}
	loader.Logger.Info().Msg("Env. file found, and loaded");
	fileEnvMap, readErr := godotenv.Read(path)
	if(readErr == nil) { 
		loader.FileEnvMap = fileEnvMap;
	}
	return nil;
}
