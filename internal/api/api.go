package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strconv"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"thundermane.com/x/daily-dad-joke/pkg/clients"
)

type MainApi struct {
	Port string
	DadJokeClient *clients.DadJokeClient
	Logger *zerolog.Logger
}

func index_handler(w http.ResponseWriter, req *http.Request) { 
	resBody, err := json.Marshal(struct{ 
		Message string `json: message`
	}{ "Welcome to my API" })
	if err != nil { 
		fmt.Fprintf(w, "Error: %v", err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(resBody)
}

type Route func(w http.ResponseWriter, req *http.Request);
type MainApiRoute func(api MainApi)Route;


var routeMap = map[string]MainApiRoute{
	"/": func(api MainApi)Route{
		return index_handler
	},
	"/random": func(api MainApi)Route{
		return func(w http.ResponseWriter, req *http.Request) {
			joke, err := api.DadJokeClient.FetchRandom(req.Context())
			if err != nil {
				fmt.Fprintf(w, "Error: %v", err)
			}
			w.Header().Set("Content-Type", "text/plain")
			w.Header().Set("Cache-Control", strconv.Itoa(60*60*24))
			fmt.Fprintf(w, joke.Message())
		}
	},
}

func (api MainApi) loadRoutes() {
	api.Logger.Info().Msg("Loading routes...")
	for key, value := range routeMap { 
		http.HandleFunc(key, value(api))
		api.Logger.Info().Msgf("%s route initialized.", key)
	}
	api.Logger.Info().Msg("Routes loaded.")
}

type MainApiStatus string;

const (
	Init MainApiStatus = "Init"
	Message = "Message"
	Active = "Active"
	Finish = "Finished"
	Error = "Error"
)

type MainApiHeartbeat struct { 
	Status MainApiStatus
	Err error
}

func (api MainApi) Listen(c chan MainApiHeartbeat) { 
	c <- MainApiHeartbeat{ Init, nil }
	if len(api.Port) < 1 { 
		api.Port = "8080"
	}
	api.loadRoutes()
	api.Logger.Info().Msgf("Listening on Port %v", api.Port)
	err := http.ListenAndServe(fmt.Sprintf(":%v", api.Port), nil)
	if err != nil { 
		api.Logger.Fatal().Err(err).Msg("There was an issue starting server")
		c <- MainApiHeartbeat{ Error, err }
	}
	c <- MainApiHeartbeat{ Active, nil }
}

func api() {
	logger := zerolog.New(os.Stderr).
		With().
		Timestamp().
		Logger()
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stdout})

	djc := clients.NewDadJokeClient(nil);
	server := MainApi{
		Port: os.Getenv("PORT"),
		DadJokeClient: djc,
		Logger: &logger,
	}

	hb := make(chan MainApiHeartbeat)
	go server.Listen(hb)

	for { 
		apiStatus := <- hb
		switch apiStatus.Status { 
		case Finish:
		case Error:
			break;
		}
	}
}